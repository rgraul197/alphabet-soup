package com.fun;

import lombok.Data;
import lombok.AllArgsConstructor;
import java.awt.Point;

/**
 * The results of a search.
 */
@AllArgsConstructor
@Data
public class SearchResult {
    /**
     * The search string
     */
    private String searchString;
    /**
     * The location of the start of the string
     */
    private Point start;
    /**
     * The location of the end of the string
     */
    private Point end;
}
