package com.fun;


import lombok.AccessLevel;
import lombok.Setter;
import lombok.Data;
import lombok.ToString;

/**
 * The in memory input.
 */
@Data
public class Input {
    /**
     * The width of the matrix
     */
    private int width;
    /**
     * The heigth of the matrix
     */
    private int heigth;
    /**
     * The charaters of the matrix
     */
    private char[][] puzzle;
    /**
     * The list of words to find.
     */
    private String words[];
}
