package com.fun;

import java.util.List;
import java.util.ArrayList;


public class SpaceLoader {

    /**
     * Linearize the search space of horizontial, verticat and diagonal into a set
     * of linera search spaces where one must search forwards and backwards
     * @param puzzle the puzzle
     * @return The search space instance
     */
    public SearchSpace loadSearchSpace(final char[][] puzzle){
        SearchSpace space = new SearchSpace();
        space.setHorizonial(loadHorizontal(puzzle));
        space.setVertical(loadVertical(puzzle));
        space.setUpDiagonal(loadUpDiagonal(puzzle));
        space.setDownDiagonal(loadDownDiagonal(puzzle));
        return space;
    }

    /**
     * Generate the horizontal strings
     * @param puzzle the puzzle
     * @return the array horizontial stringss
     */
    private String[] loadHorizontal(final char[][] puzzle) {
        List<String> horizontal = new ArrayList<String>();
        for (int i = 0; i < puzzle[0].length; i++) {
            horizontal.add(new String(puzzle[i]));
        }
        return horizontal.toArray(new String[horizontal.size()]);
    }

    /**
     * Loads the vertical strings
     * @param puzzle the puzzle
     * @return the array of vertical stringss
     */
    private String[] loadVertical(final char[][] puzzle) {
        List<String> vertial = new ArrayList<String>();
        char colArray[] = new char[puzzle[0].length];
        for (int i = 0; i < puzzle.length; i++) {
            for (int row = 0; row < puzzle[0].length; row++) {
                colArray[row] = puzzle[row][i];
            }
            vertial.add(new String(colArray));
        }
        return vertial.toArray(new String[vertial.size()]);
    }

    /**
     * Build the list of upward diagonal strings
     * @param puzzle the puzzle
     * @return the array of strings
     */
    private String[] loadUpDiagonal(final char[][] puzzle) {
        List<String> diagonal = new ArrayList<String>();
        //Add top half
        for (int i = 0; i <  puzzle.length; i++) {
            char colArray[] = new char[i + 1];
            for (int row = 0; row < i + 1 ; row++) {
                colArray[row] = puzzle[i-row][row];
            }
            diagonal.add(new String(colArray));
        }
        //Load bottom half
        for (int i = 1; i <  puzzle.length; i++) {
            char colArray[] = new char[puzzle.length - i];
            int row =4;
            for (int col = i; col < puzzle.length; col++) {
                colArray[col -i] = puzzle[row ][col];
                row--;
            }
            diagonal.add(new String(colArray));
        }
        return diagonal.toArray(new String[diagonal.size()]);
    }

    /**
     * Build the list of downward diagonal strings
     * @param puzzle the puzzle
     * @return the array of strings
     */
    private String[] loadDownDiagonal(final char[][] puzzle) {
        List<String> diagonal = new ArrayList<String>();
        //Add top half
        for (int i =  puzzle.length - 1; i > -1; i--) {
            char colArray[] = new char[puzzle.length - i];
            int shift = 0;
            for (int row = 0; row < puzzle.length - i  ; row++) {
                colArray[row] = puzzle[i + shift][row];
                shift++;
            }
            diagonal.add(new String(colArray));
        }
        //Load top half
        for (int i = 0; i <  puzzle.length - 1; i++) {
            char colArray[] = new char[puzzle.length - (i + 1)];
            int col =0;
            for (int row = i +1; row < puzzle.length ; row++) {
                colArray[col] = puzzle[col ][row];
                col++;
            }
            diagonal.add(new String(colArray));
        }
        return diagonal.toArray(new String[diagonal.size()]);
    }
}
