package com.fun;

import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.awt.Point;

public class SpaceSearcher {

    /**
     * Search all the spaces for words.  If a word if found in a search space remove it from the subsequent searches
     * @param space The different serch spaces
     * @param words the words to seaarch for
     * @return the List of results
     */
    public List<SearchResult> findStrings(final SearchSpace space, final String words[] ) {
        List<SearchResult> results = new ArrayList<SearchResult>();
        ArrayList<String> remainingWords = new ArrayList();
        remainingWords.addAll(Arrays.asList(words));
        results.addAll(findStrings(space.horizonial, remainingWords, Direction.Horizontial, space.horizonial.length ));
        for (SearchResult result: results) {
            remainingWords.remove(result.getSearchString());
        }
        List<SearchResult> verticalResults = findStrings(space.vertical, remainingWords, Direction.Vertical, space.horizonial.length );
        for (SearchResult result: verticalResults) {
            remainingWords.remove(result.getSearchString());
        }
        results.addAll(verticalResults);
        List<SearchResult> diagonalResults = findStrings(space.upDiagonal, remainingWords, Direction.UpDiagonal, space.horizonial.length );
        for (SearchResult result: diagonalResults) {
            remainingWords.remove(result.getSearchString());
        }
        results.addAll(diagonalResults);
        List<SearchResult> downDiagonalResults = findStrings(space.downDiagonal, remainingWords, Direction.DownDiagonal, space.horizonial.length );
        results.addAll(downDiagonalResults);
        return results;
    }

    /**
     * Search all the spaces
     * @param space The collection of spaces
     * @param words the array of words to search
     * @param direction the direction off the search
     * @param size the size of the matrix
     *             Note could have been found by the max line length of digonals. but seemed just as easy to passit in.
     * @return
     */
    private List<SearchResult> findStrings(final String space[], final List<String> words, Direction direction , final int size) {
        List<SearchResult> results = new ArrayList<SearchResult>();
        for (String word : words ) {
            int lineIndex = -1;
            int startIndex = -1;
            boolean reversed = false;
            for (String line : space) {
                lineIndex++;
                if (line.length() >= word.length()) {
                    startIndex = line.indexOf(word);
                    if (startIndex > -1) {
                        break;
                    }
                    StringBuilder builder = new StringBuilder(word);
                    startIndex = line.indexOf(builder.reverse().toString());
                    if (startIndex > -1) {
                        reversed = true;
                        break;
                    }
                }
            }
            //Now found map the result in to the correct space (Inverese mapping)
            if (startIndex > -1) {
                switch (direction) {
                    case Horizontial:
                        if (reversed) {
                            results.add(new SearchResult(word, new Point(lineIndex, startIndex + word.length()-1), new Point(lineIndex, startIndex)));
                        } else {
                            results.add(new SearchResult(word, new Point(lineIndex, startIndex), new Point(lineIndex, startIndex + word.length()-1)));
                        }
                        break;
                    case Vertical:
                        if (reversed) {
                            results.add(new SearchResult(word, new Point(startIndex + word.length()-1, lineIndex), new Point(startIndex, lineIndex)));
                        } else {
                            results.add(new SearchResult(word, new Point(startIndex, lineIndex), new Point(startIndex + word.length() -1, lineIndex)));
                        }
                        break;
                    case UpDiagonal:
                        if (lineIndex > size) {
                            lineIndex -= (size -1);
                            startIndex =(size -1) -startIndex;
                            if (reversed) {
                                results.add(new SearchResult(word, new Point(lineIndex, startIndex ), new Point(lineIndex + (word.length() -1), startIndex - (word.length() -1))));
                            } else {
                                results.add(new SearchResult(word, new Point(startIndex, lineIndex), new Point(startIndex - (word.length()-1), lineIndex + (word.length()-1))));
                            }
                        } else {
                            startIndex = 0;
                            if (reversed) {
                                results.add(new SearchResult(word, new Point(startIndex, lineIndex ), new Point(startIndex + (word.length() -1), lineIndex - (word.length() -1))));
                            } else {
                                results.add(new SearchResult(word, new Point(lineIndex, startIndex), new Point(lineIndex - (word.length()-1), startIndex + (word.length()-1))));
                            }
                        }
                        break;
                    case DownDiagonal:
                        if (lineIndex > size) {
                            lineIndex -= (size -1);
                            if (reversed) {
                                results.add(new SearchResult(word, new Point(lineIndex, startIndex ), new Point(lineIndex + (word.length() -1), startIndex - (word.length() -1))));
                            } else {
                                results.add(new SearchResult(word, new Point(startIndex, lineIndex), new Point(startIndex + (word.length()-1), lineIndex + (word.length()-1))));
                            }
                        } else {
                            lineIndex = (size -1) - lineIndex + startIndex;
                            if (reversed) {
                                results.add(new SearchResult(word, new Point(startIndex, lineIndex), new Point(startIndex + (word.length() -1), lineIndex + (word.length() -1))));
                            } else {
                                results.add(new SearchResult(word, new Point(lineIndex, startIndex), new Point(lineIndex + (word.length()-1), startIndex + (word.length()-1))));
                            }
                        }
                        break;
                    default:
                        //bad coding inut wrong
                }
            }
        }
        return results;
    }
}
