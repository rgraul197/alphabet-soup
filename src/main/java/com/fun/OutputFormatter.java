package com.fun;

import java.util.List;
import java.util.Optional;

/**
 * Simple class to Output the results
 */
public class OutputFormatter {

    /**
     * Results are to be output in the order of the input so input myst be presented
     * @param words the words to search for in the order received
     * @param results the results of the search
     * @return The output of the results as a string.
     */
    public String format(final String words[], final List<SearchResult> results) {
        StringBuilder builder = new StringBuilder();
        for (String word : words) {
            Optional<SearchResult> result = findByString(results, word);
            if (!result.isEmpty()) {
                builder.append(format(result.get()));
            } else {
                System.out.println("Result " + word + " was not found.");
            }
        }
        return builder.toString();
    }

    /**
     * Lambda function to find the results foe an input work.
     * @param results the found results.
     * @param searchWord the word that was to be searched
     * @return the SearchResult if found
     */
    private Optional<SearchResult> findByString(final List<SearchResult> results, final String searchWord) {
        return results.stream().filter(p -> p.getSearchString().equals(searchWord)).findAny();
    }

    /**
     * Format the result in the desired output format.
     * @param result he result of a single search
     * @return The formatted line
     */
    private String format(SearchResult result) {
        StringBuilder builder = new StringBuilder();
        builder.append(result.getSearchString());
        builder.append(" ");
        builder.append(result.getStart().x);
        builder.append(":");
        builder.append(result.getStart().y);
        builder.append(" ");
        builder.append(result.getEnd().x);
        builder.append(":");
        builder.append(result.getEnd().y);
        builder.append("\n");
        return builder.toString();
    }
}
