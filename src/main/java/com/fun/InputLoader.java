package com.fun;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.awt.Point;

import com.fun.exception.InvalidInputException;

/**
 * Simple clss to reade th input file and load it into memory.
 */
public class InputLoader {

    /**
     * Load the input file into a workable in memory structure Input
     *
     * @param filename The file name to read
     * @return The populated input
     * @throws IOException           If the input file could  not be found
     * @throws InvalidInputException If the input file is misconfigured.
     */
    public Input loadInput(String filename) throws IOException, InvalidInputException {
        Input input = new Input();
        List<String> allLines = Files.readAllLines(Paths.get(filename));
        Point sizePoint = getMatrixSize(allLines.get(0));
        input.setHeigth(sizePoint.y);
        input.setWidth(sizePoint.x);
        String puzzleLine[] = new String[sizePoint.x];
        char[][] puzzle = new char[sizePoint.y][sizePoint.x];
        for (int i = 0; i < sizePoint.y; i++) {
            puzzleLine = allLines.get(i + 1).split(" ");
            for (int j = 0; j < sizePoint.y; j++) {
                puzzle[i][j] = puzzleLine[j].toCharArray()[0];
            }

        }
        input.setPuzzle(puzzle);
        List<String> words = new ArrayList<>();
        for (int i = sizePoint.y + 1; i < allLines.size(); i++) {
            if (allLines.get(i).trim().length() > 0) {
                words.add(allLines.get(i));
            }
        }
        input.setWords(words.toArray(new String[words.size()]));
        return input;
    }

    /**
     * Parse out the matric dimensions
     * input is mxm
     *
     * @param firstLine The first line of the input function
     * @return the point containing the m and n matrix size.
     * @throws InvalidInputException
     */
    private Point getMatrixSize(String firstLine) throws InvalidInputException {
        String parts[] = firstLine.split("x");
        if (parts.length != 2) {
            throw new InvalidInputException(String.format("The format of  firstLine [%s] is invalid -- no x", firstLine));
        } else {
            try {
                return new Point(Integer.parseInt(parts[0]), Integer.parseInt(parts[1]));
            } catch (NumberFormatException e) {
                throw new InvalidInputException(String.format("The format of  firstLine [%s] is invalid -- %s", firstLine, e.getMessage()));
            }
        }
    }
}
