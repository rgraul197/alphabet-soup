package com.fun;

/**
 * Enum defining the possible directions for the word in the grid
 */
public enum Direction {
    Horizontial,
    Vertical,
    UpDiagonal,
    DownDiagonal
}
