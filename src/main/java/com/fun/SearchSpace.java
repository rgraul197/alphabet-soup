package com.fun;

/**
 * The original word search involves searching from a starting point in the matrix in 8 dimensions
 * So to linearie the problem set flatten the search space by making 4 vector spaces (horizonial, vertical, updiagonal, downDiagonal)
 * and cover the last 2 by reversing the string.  In this way we only have to do linear searches in the spaces and then
 * map them to output  This class simply holds the various search spaces.
 */

import lombok.Data;
@Data
public class SearchSpace {
    /**
     * the horizonial space
     */
    String[] horizonial;
    /**
     * the vertical space
     */
    String[] vertical;
    /**
     * the up diagonal space
     */
    String[] upDiagonal;
    /**
     * the down diagonal space
     */
    String[] downDiagonal;
}
