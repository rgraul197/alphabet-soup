package com.fun;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import com.fun.exception.InvalidInputException;

/**
* Main entry class
*/
public class WordSearcher {
    /**
     * Main entry method
     * @param args the arguments for the application
     */
    public static void main(String[] args) {
        try {
            Path currentRelativePath = Paths.get("./src/main/resources/Sample.txt");
            InputLoader loader = new InputLoader();
            Input input = loader.loadInput(currentRelativePath.toAbsolutePath().toString());
            SpaceLoader spaceLoader = new SpaceLoader();
            SearchSpace space = spaceLoader.loadSearchSpace(input.getPuzzle());
            SpaceSearcher searcher = new SpaceSearcher();
            List<SearchResult> results = searcher.findStrings(space, input.getWords());
            OutputFormatter formatter = new OutputFormatter();
            System.out.println(formatter.format(input.getWords(), results));
        } catch (InvalidInputException e) {

        } catch (IOException e) {

        }
    }
}
