package com.fun;

import org.junit.jupiter.api.Test;
import java.nio.file.Paths;
import java.nio.file.Path;

/**
 * Incomplete test as scratch pad for verifying inut loading.
 */
public class InputLoaderTest {
    @Test
    public void testload() throws Exception {
        Path currentRelativePath = Paths.get("./src/main/resources/Sample.txt");
        String s = currentRelativePath.toAbsolutePath().toString();
        System.out.println("Current relative path is: " + s);
        String filename = "./src/msin/resources/Sample.txt";
        InputLoader loader = new InputLoader();
        Input input = loader.loadInput(currentRelativePath.toAbsolutePath().toString());
        System.out.println(input);
    }
}
