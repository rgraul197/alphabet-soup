package com.fun;

import org.junit.jupiter.api.Test;
import java.nio.file.Paths;
import java.nio.file.Path;

/**
 * Incomplete test as scratch pad for verifying generating search spaces.
 */
public class SpaceLoaderTest {
    @Test
    public void testload() throws Exception {
        Path currentRelativePath = Paths.get("./src/main/resources/Sample.txt");
        InputLoader loader = new InputLoader();
        Input input = loader.loadInput(currentRelativePath.toAbsolutePath().toString());
        SpaceLoader spaceLoader = new SpaceLoader();
        SearchSpace space = spaceLoader.loadSearchSpace(input.getPuzzle());
        System.out.println(space);
    }
}
