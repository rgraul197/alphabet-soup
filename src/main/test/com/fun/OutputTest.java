package com.fun;

import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Incomplete test as scratch pad for verifying output formatting.
 */
public class OutputTest {

    @Test
    public void testload() throws Exception {
        Path currentRelativePath = Paths.get("./src/main/resources/Sample.txt");
        InputLoader loader = new InputLoader();
        Input input = loader.loadInput(currentRelativePath.toAbsolutePath().toString());
        SpaceLoader spaceLoader = new SpaceLoader();
        SearchSpace space = spaceLoader.loadSearchSpace(input.getPuzzle());
        SpaceSearcher searcher = new SpaceSearcher();
        List<SearchResult> results = searcher.findStrings(space,input.getWords() );
        OutputFormatter formatter = new OutputFormatter();
        System.out.println(formatter.format(input.getWords(), results));
    }
}
